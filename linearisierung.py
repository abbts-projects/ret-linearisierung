import pandas as pd
import glob
import matplotlib.pyplot as plt
import seaborn as sns

class jumpGenerator:
    def __init__(self):
        pass

    def generate(self, time=20, start=0, end=200, steps=50, timePerStep=True):
        """Generates a table with jumps
        
        Keyword Arguments:
            time {int} -- Time in seconds to hold a signal (default: {20})
            start {int} -- Start value for the input signal (default: {0})
            end {int} -- End value for the input signal (default: {200})
            steps {int} -- How many steps should be done between start and end (default: {50})
            timePerStep {bool} -- If the set time is to be set per step or over all steps (default: {True})
        
        Returns:
            DataFrame -- Dataframe containing all the step data
        """
        allSteps = []
        
        stepSize = round((end-start)/steps)
        signalRange = range(start, end+stepSize, stepSize)

        if timePerStep:
            timeStep = time
        else:
            timeStep = time/len(signalRange)
        
        for i, sig in enumerate(signalRange):
            allSteps.append((timeStep*i, sig))
            allSteps.append((timeStep*(i+1), sig))

        simTime = len(signalRange) * timeStep
        print(f"Set the simulation time to: {simTime}")

        return pd.DataFrame(allSteps)
    
    def save(self, df):
        """Save the step dataframe to a txt file
        
        Arguments:
            df {DataFrame} -- Dataframe containing the step data
        
        Returns:
            str -- Path to the save file
        """
        # Save it as a csv file
        file = "./jumps.txt"

        # And only save the rows where it jumps!
        df.to_csv(file, header=False, sep="\t", index=False)

        return file

class Linearisierung:
    class Columns:
        jumps = "Jumps"
        willJump = "WillJump"

        out = "Out"
        outNormalized = "OutNormalized"

        time = "Zeit"

    def __init__(self):
        # Prepare
        self.path = None
        self.plotData = True

        # Seaborn style
        sns.set(style="whitegrid")

    def work(self, path=None):
        """Will load the exported file and evaluate it + do a plot
        
        Keyword Arguments:
            path {[type]} -- [description] (default: {None})
        """
        # Check if we have a file
        if path is None:
            files = glob.glob("*.CSV")

            try:
                self.path = files[0]

            except:
                print("Failed to find any .csv files!")
                return

        # Work the file
        df = self.__loadFile(self.path)
        df = self.__normalize(df)
        dfCorrected = self.__correction(df)
        outFile = self.__saveXYFile(dfCorrected)

        print(f"Done: {outFile}")

        if self.plotData:
            # Do a plot
            self.__plotData(df)

    def __loadFile(self, path):
        """Loads a CSV file exported from boris
        
        Arguments:
            path {str} -- Path to the file
        
        Returns:
            DataFrame -- A data frame with all the loaded data from boris
        """
        self.path = path

        # Load the file
        df = pd.read_csv(self.path, sep="\t")
        df = df.rename(columns=lambda x: x.strip())

        colType = {}
        for col in list(df):
            try:
                df[col] = df[col].str.replace(" ", "")
                df[col] = df[col].str.replace(",", ".")
                colType[col] = float
            except:
                pass

        # Convert to float
        df = df.astype(colType)

        # Do some rounding
        df[self.Columns.jumps] = round(df[self.Columns.jumps] * 1000) / 1000

        # Get the jumps
        df[self.Columns.willJump] = df[self.Columns.jumps].diff() > 0
        df[self.Columns.willJump] = df[self.Columns.willJump].shift(-1, fill_value=True)

        return df

    def __normalize(self, df):
        """Will normalize the signal data
        
        Arguments:
            df {DataFrame} -- A dataframe containing all the data
        
        Returns:
            DataFrame -- A dataframe with an additional colum contining the normalized values
        """
        # Get the last measured value
        # This is the "max" value
        lastValue = df.iloc[-1][self.Columns.out]

        # Ony use the values, where there was a jump
        df[self.Columns.outNormalized] = df[self.Columns.out] / lastValue * 100

        # Return it
        return df

    def __plotData(self, df):
        """Will plot all the data in the given dataframe
        
        Arguments:
            df {DataFrame} -- The data to plot. The time Colum will be mapped to the y axis
        """
        fig, ax = plt.subplots(1, 1)

        # Get the header, remove the time colum (x axis)
        header = list(df)
        header.remove(self.Columns.time)
        header.remove(self.Columns.willJump)

        # Loop each title
        for title in header:
            sns.lineplot(x=self.Columns.time, y=title, data=df, label=title, ax=ax)

        # Add the jump points
        jumps = df[df[self.Columns.willJump]]
        sns.scatterplot(x=self.Columns.time, y=self.Columns.out, data=jumps, ax=ax)

        # Show the plot
        plt.show()

    def __correction(self, df):
        """Returns the corrections
        
        Arguments:
            df {DataFrame} -- A dataframe with all the table data
        
        Returns:
            DataFrame -- A DataFrame, which contains the corrections needed
        """
        # Only select rows where we jump
        df = df[df[self.Columns.willJump]]

        # Get the axis
        x_norm = df[self.Columns.outNormalized]
        y = df[self.Columns.jumps]

        ## Return the data
        return pd.DataFrame({"x": x_norm, "y": y})

    def __saveXYFile(self, df):
        """Saves the data to a csv file, so we can then again read it in boris
        
        Arguments:
            df {DataFrame} -- The data Frame containing the correctional data
        
        Returns:
            str -- Path to the saved file
        """
        # Save it as a csv file
        file = "./linearization.XY"

        # Drop duplicates in the x axis
        df.drop_duplicates(subset="x", keep=False, inplace=True)

        # And only save the rows where it jumps!
        df.to_csv(file, header=False, sep="\t", index=False)

        return file


if __name__ == "__main__":
    jg = jumpGenerator()
    df = jg.generate()
    jg.save(df)

    l = Linearisierung()
    l.plotData = True
    l.work()
